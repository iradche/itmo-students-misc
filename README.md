# itmo-students-misc
Different materials for students of ITMO University

[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/iradche/itmo-students-misc/master?grs=github&t=white)

# Description

* [pub](https://github.com/iradche/itmo-students-misc/tree/master/pub) Materials for the publication
* [useful](https://github.com/iradche/itmo-students-misc/tree/master/useful) Useful links, etc
* [PITCHME.md](https://github.com/iradche/itmo-students-misc/blob/master/PITCHME.md) Presentation
* [Issues](https://github.com/iradche/itmo-students-misc/issues) Topics for research
