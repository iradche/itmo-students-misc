* [ITMOtemplate_ru.ppt](https://github.com/iradche/itmo-students-misc/blob/master/pub/ITMOtemplate_ru.ppt) -- Template of ITMO University for the presentations in Russian
* [journals-list.md](https://github.com/iradche/itmo-students-misc/blob/master/pub/journals-list.md) -- Open Access Springer Journals
* [Перечень ВАК 03.06.2016.pdf](https://github.com/iradche/itmo-students-misc/blob/master/pub/%D0%9F%D0%B5%D1%80%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%20%D0%92%D0%90%D0%9A%2003.06.2016.pdf) -- перечень рецензируемых научных изданий, в которых должны быть опубликованы основные
научные результаты диссертаций 


